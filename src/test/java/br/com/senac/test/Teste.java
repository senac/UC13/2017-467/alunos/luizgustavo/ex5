/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;


import br.com.senac.Principal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class Teste {

    public Teste() {
    }
   @Test 
   public void numero2Par(){
      int numero = 2;
      boolean resultado = Principal.par(numero);
       assertTrue(resultado);
   }
   @Test
   public void numero2Positivo(){
       int numero = 2;
       boolean resultado = Principal.positivo(numero);
       assertTrue(resultado);
       
   }
   @Test
   public void numero3NaoPar(){
       int numero = 3;
       boolean resultado = Principal.par(numero);
       assertFalse(resultado);
   }
   @Test
   public void numero3NaoPositivo(){
       int numero = -3;
       boolean resultado = Principal.positivo(numero);
       assertFalse(resultado);
   }
   
}
