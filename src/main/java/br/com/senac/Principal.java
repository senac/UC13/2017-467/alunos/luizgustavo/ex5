/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

/*
Faça um programa que leia um número inteiro e mostre uma mensagem indicando se
este número é par ou ímpar, e se é positivo ou negativo.

 */
public class Principal {
    
    public static boolean par(int numero){
        
        return numero % 2 == 0;
    }
    
    public static boolean positivo(int numero){
       
        return numero > 0;
    }
}
